//
//  main.m
//  MoJoTweet
//
//  Created by MaddArkitekt on 4/13/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TWTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TWTAppDelegate class]));
    }
}
