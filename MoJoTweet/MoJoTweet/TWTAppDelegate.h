//
//  TWTAppDelegate.h
//  MoJoTweet
//
//  Created by MaddArkitekt on 4/13/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TWTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
