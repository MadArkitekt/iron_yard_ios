//
//  CARBrake.m
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CARBrake.h"

@implementation CARBrake

- (id)init
{
    self = [super init];
    if (self) {
        self.quality = @"Squeeky";
        self.backgroundColor = [UIColor redColor];
        self.hydraulic = NO;
    }
    return self;
}


@end
