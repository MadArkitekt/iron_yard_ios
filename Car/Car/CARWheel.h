//
//  CARWheel.h
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CARWheel : UIView

@property (nonatomic) int tirePressure;
@property (nonatomic) BOOL flat;
@property (nonatomic) NSString *maker;
@property (nonatomic) NSString *condition;
@property (nonatomic) UIColor *backGroundColor;

- (void)fillWithAir;

@end
