//
//  CARIgnition.h
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CARIgnition : UIButton

@property (nonatomic) BOOL running;
@property (nonatomic) UIColor *backGroundColor;

@end
