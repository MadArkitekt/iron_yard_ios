//
//  CARWindow.h
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CARWindow : UIView
@property (nonatomic) BOOL open;
@property (nonatomic) int width;
@property (nonatomic) int height;
@property (nonatomic) BOOL crack;
@property (nonatomic) UIColor *backGroundColor;

@end
