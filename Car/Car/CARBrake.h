//
//  CARBrake.h
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CARBrake : UIButton
@property (nonatomic) NSString  *quality;
@property (nonatomic) BOOL hydraulic;
@property (nonatomic) UIColor *backGroundColor;


//hydraulic = YES
//drum or other = NO

@end
