//
//  CARBumper.m
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CARBumper.h"

@implementation CARBumper

- (id)init
{
    self = [super init];
    if (self) {
        self.material = @"dust";
        self.condition = @"nonexistant";
        self.width = 4.5;
        self.backgroundColor = [UIColor greenColor];
        
        
        
    }
    return self;
}

@end
