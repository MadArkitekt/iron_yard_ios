//
//  CARBumper.h
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CARBumper : UIView

@property (nonatomic) float width;
@property (nonatomic) NSString *condition;
@property (nonatomic) NSString *material;
@property (nonatomic) UIColor *backGroundColor;
@end
