//
//  CARWheel.m
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CARWheel.h"

@implementation CARWheel

- (id)init
{
    self = [super init];
    if (self) {
        self.tirePressure = 30;
        self.condition = @"Poor";
        self.maker = @"Firestone";
        self.flat = NO;
        self.backgroundColor = [UIColor blackColor];
        self.layer.cornerRadius = 20;
    
    }
    
    return self;
}


- (void)fillWithAir
{
    self.tirePressure = 30;
    self.flat = NO;
}

@end
