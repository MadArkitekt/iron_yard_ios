//
//  CARWindow.m
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CARWindow.h"

@implementation CARWindow

- (id)init {
    self = [super init];
    if (self) {
        self.width = 11;
        self.height = 50;
        self.crack = NO;
        }
    return self;
}

@end
