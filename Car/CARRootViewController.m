//
//  CARRootViewController.m
//  Car
//
//  Created by MaddArkitekt on 4/1/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CARRootViewController.h"
#import "CARIgnition.h"
#import "CARGasPedal.h"
#import "CARBumper.h"
#import "CARBrake.h"
#import "CARWindow.h"
#import "CARWheel.h"


@interface CARRootViewController ()


@end

@implementation CARRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CARWheel *wheel1 = [[CARWheel alloc] init];
    wheel1.frame = CGRectMake(40, 300, 25, 25);
    wheel1.tirePressure = 20;
    [self.view addSubview:wheel1];
    
    CARWheel *wheel2 = [[CARWheel alloc] init];
    wheel2.frame = CGRectMake(80, 300, 25, 25);
    wheel2.tirePressure = 24;
    [self.view addSubview:wheel2];
    
    
    CARWheel *wheel3 = [[CARWheel alloc] init];
    wheel3.frame = CGRectMake(140, 300, 50, 50);
    [self.view addSubview:wheel3];
    
    CARWheel *wheel4 = [[CARWheel alloc] init];
    wheel4.frame = CGRectMake(230, 300, 50, 50);
    [self.view addSubview:wheel4];
    
    //Illustrates gas
    CARGasPedal * gasPedal = [[CARGasPedal alloc] init];
    gasPedal.frame = CGRectMake(200, 400, 100, 120);
    [self.view addSubview:gasPedal];
    [gasPedal setTitle:@"GO, BABY!!!"forState:UIControlStateNormal];
    [gasPedal addTarget:self action:@selector(pressGasPedal)
       forControlEvents:UIControlEventTouchUpInside];

    //Illustrates brake
    CARBrake * brake = [[CARBrake alloc] init];
    brake.frame = CGRectMake(30, 400, 100, 120);
    [self.view addSubview:brake];
    [brake setTitle:@"STOP!!!"forState:UIControlStateNormal];
    [brake addTarget:self action:@selector(pressBrake)
       forControlEvents:UIControlEventTouchUpInside];
    
    CARWindow * window = [[CARWindow alloc] init];
    window.frame = CGRectMake(150, 235, 110, 50);
    window.backgroundColor = [UIColor blackColor];
    window.alpha = 0.2;
    [self.view addSubview:window];
    
    CARIgnition* ignition = [[CARIgnition alloc] init];
    ignition.frame = CGRectMake(140, 450, 50, 50);
    [self.view addSubview:ignition];
    [ignition setTitle:@"Crank"forState:UIControlStateNormal];
    [ignition addTarget:self action:@selector(pressIgnition)
    forControlEvents:UIControlEventTouchUpInside];

}

-(void)pressGasPedal

{
    
    NSLog(@"Amidst the clouds of smoking rubber....he was gone...");
    
}
-(void)pressIgnition
{
    
    NSLog(@"Rumble..bub, bub, bub, bub...VROOOOM!!");
    
}
-(void)pressBrake
{
    
    NSLog(@"SCREEEEEECH!!!!");
    
}

//-(void)pressBrake

-(void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    }

@end
