//
//  PNAAppDelegate.h
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
