//
//  PNAPixelSounds.h
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@interface PNAPixelSounds : NSObject
//@property (nonatomic) NSMutableArray *players;
@property (nonatomic,) AVAudioPlayer *player;

- (void)playSoundWithName:(NSString *)soundName;
@end
