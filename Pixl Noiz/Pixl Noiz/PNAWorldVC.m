//
//  PNAWorldVC.m
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//
/* For physical interaction introduction - gravity...collisions */
#import "PNAWorldVC.h"

@interface PNAWorldVC () <UICollisionBehaviorDelegate, UIGestureRecognizerDelegate>

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIPushBehavior *pusher;
@property (nonatomic) UICollisionBehavior *collider;
@property (nonatomic) UIDynamicItemBehavior *boxDyamProps;
@property (nonatomic) UIAttachmentBehavior *attacher;
@property (nonatomic) NSMutableArray *boxes;
@property (nonatomic) UIGestureRecognizer *touch;

@end

@implementation PNAWorldVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.boxes = [@[]mutableCopy];
        self.touches= [@[]mutableCopy];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    // Get the specific point that was touched
    CGPoint loc = [touch locationInView:self.view];
    NSLog(@"X location: %f", loc.x);
    NSLog(@"Y Location: %f",loc.y);
    [self createBox];
}



-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    [self createBox];
    UITouch *touch = [touches anyObject];
    
    CGPoint loc = [touch locationInView:self.view];
    
// float x = loc.x;
//float y = loc.y;
   
    
    }
    - (void)interact
    {
        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
        [self createPaddle];
        
        self.collider = [[UICollisionBehavior alloc] initWithItems:[self. llItems]];
        self.collider.collisionDelegate = self;
        
        self.collider.collisionMode = UICollisionBehaviorModeEverything;
        
        int w = self.view.frame.size.width;
        int h = self.view.frame.size.height;
        
        [self.collider addBoundaryWithIdentifier:@"ceiling" fromPoint:CGPointMake(0, 0) toPoint:CGPointMake(w, 0)];
        [self.collider addBoundaryWithIdentifier:@"leftWall" fromPoint:CGPointMake(0, 0) toPoint:CGPointMake(0, h)];
        [self.collider addBoundaryWithIdentifier:@"rightWall" fromPoint:CGPointMake(w, 0) toPoint:CGPointMake(w, h)];
        [self.collider addBoundaryWithIdentifier:@"floor" fromPoint:CGPointMake(0, h + 10) toPoint:CGPointMake(w, h + 10)];
        [self.animator addBehavior:self.collider];
        
        self.boxDynamicProperties = [self createPropertiesForItems:self.box];
        
    }
- (void)createBox;
{
    UIView *box = [[UIView alloc] initWithFrame:CGRectMake(loc.x, loc.y, 10, 10)];
    
    UITouch *touch = [touches anyObject];
    
    CGPoint loc = [touch locationInView:self.view];

    box.backgroundColor = [UIColor whiteColor];

}
- (NSArray *)allItems
{
    NSMutableArray *items = [@[self.boxes] mutableCopy];
    return items;
}

        -(BOOL)prefersStatusBarHidden
        { return YES; }
        
        - (void)didReceiveMemoryWarning
        {
            [super didReceiveMemoryWarning];
            // Dispose of any resources that can be recreated.
        }
        
        @end;
