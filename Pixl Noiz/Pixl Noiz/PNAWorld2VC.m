//
//  PNAWorld2VC.m
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/6/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "PNAWorld2VC.h"
#import "PNAPixelSounds.h"

@interface PNAWorld2VC () <UICollisionBehaviorDelegate>
// The below need only be init'd ONCE
@property (nonatomic) UIDynamicAnimator *animator; //makes everything look like it's moving
@property (nonatomic) UIGravityBehavior *gravity; //adds gravity
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIDynamicItemBehavior *shardBehavior;
@property (nonatomic) UICollisionBehavior *shardCollision;
@end

@implementation PNAWorld2VC
{
    PNAPixelSounds *sounds;
    NSArray *splatterDirections;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        sounds = [[PNAPixelSounds alloc] init];
        splatterDirections = @[
                               [NSValue valueWithCGPoint:CGPointMake(-0.1, -0.1)],
                               [NSValue valueWithCGPoint:CGPointMake(-0.05, -0.1)],
                               [NSValue valueWithCGPoint:CGPointMake(0.0, -0.1)],
                               [NSValue valueWithCGPoint:CGPointMake(0.05, -0.1)],
                               [NSValue valueWithCGPoint:CGPointMake(0.1, -0.1)] // CGVectorMake(0.0, -1.0);
                               ];
        
        
        self.shardBehavior = [[UIDynamicItemBehavior alloc] init];
        
        [self.animator addBehavior:self.shardBehavior];
        self.shardBehavior.density = 10;
        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
        
        self.gravity = [[UIGravityBehavior alloc] init];
        [self.animator addBehavior:self.gravity];
        
        self.collision = [[UICollisionBehavior alloc] init];
        self.collision.translatesReferenceBoundsIntoBoundary = YES; //Need to set boundary, getting reference view from self.animator init above
        self.collision.collisionDelegate = self;
        [self.animator addBehavior:self.collision];
        
        self.shardCollision = [[UICollisionBehavior alloc] init];
        self.shardCollision.translatesReferenceBoundsIntoBoundary = YES; //Need to set boundary, getting reference view from self.animator init above
        self.shardCollision.collisionDelegate = self;

        [self.animator addBehavior:self.shardCollision];

        
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
        [self createBlockWithLocation:[touch locationInView:self.view]];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
        [self createBlockWithLocation:[touch locationInView:self.view]];
}

- (void)createBlockWithLocation:(CGPoint)location
{
    UIView *block = [[UIView alloc] initWithFrame:CGRectMake(location.x, location.y, 20, 20)];
    block.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:block];
    [self.gravity addItem:block];
    [self.collision addItem:block];

    
}

- (void)createShardsWithLocation:(CGPoint)location
{
    for (NSValue *pointValue in splatterDirections)
    {
        CGPoint direction = [pointValue CGPointValue];
        
        UIView *shard = [[UIView alloc] initWithFrame:CGRectMake(location.x + direction.x * 200, location.y - 50, 10, 10)];
        shard.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:shard];

        [self.gravity addItem:shard];
        [self.shardCollision addItem:shard];
        [self.shardBehavior addItem:shard];
        
        UIPushBehavior *pusher = [[UIPushBehavior alloc] initWithItems:@[shard] mode:UIPushBehaviorModeInstantaneous];
        
        [self.animator addBehavior:pusher];
        
        pusher.pushDirection = CGVectorMake(direction.x, direction.y);
    }
}

-(void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p
{
    if ([behavior isEqual:self.collision])
    {
    [sounds playSoundWithName:@"snare"];
    [self createShardsWithLocation:p];
    
    UIView *collidedBlock = (UIView *)item;
    [self.gravity removeItem:collidedBlock];
    [self.collision removeItem:collidedBlock];
    [collidedBlock removeFromSuperview];
    
}
if ([behavior isEqual:self.shardCollision])
{
//    [sounds playSoundWithName:@"snare"];

    UIView *collidedShard = (UIView *)item;
    [self.gravity removeItem:collidedShard];
    [self.collision removeItem:collidedShard];
    [collidedShard removeFromSuperview];
    
}
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
