//
//  PNAPixelSounds.m
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "PNAPixelSounds.h"

@implementation PNAPixelSounds
- (void)playSoundWithName:(NSString *)soundName
{
    //fileData
    NSData *fileData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:soundName ofType:@"wav"]]; //to make dynamic
    self.player = [[AVAudioPlayer alloc] initWithData:fileData error:nil];
 
    [self.player play];
    
}

@end
