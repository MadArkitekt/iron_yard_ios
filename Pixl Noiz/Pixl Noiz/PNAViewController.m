//
//  PNAViewController.m
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "PNAViewController.h"
#import "PNAPixelSounds.h"
@interface PNAViewController ()

@end

@implementation PNAViewController
{
    PNAPixelSounds *sounds;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.view.backgroundColor = [UIColor colorWithWhite:0.15 alpha:1.0];
        sounds = [[PNAPixelSounds alloc] init];
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [sounds playSoundWithName: @"crunk"];
    NSLog(@"Play Sound");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)prefersStatusBarHidden { return YES; };
@end
