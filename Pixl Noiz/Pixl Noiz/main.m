//
//  main.m
//  Pixl Noiz
//
//  Created by MadArkitekt on 5/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PNAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PNAAppDelegate class]));
    }
}
