//
//  FSRequest.h
//  Random Destination
//
//  Created by Edward Salter on 6/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSRequest : NSObject
+(NSArray *)getVenuesWithLat:(double)latitude andLong:(double) longitude;
@end
