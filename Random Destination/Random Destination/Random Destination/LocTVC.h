//
//  LocTVC.h
//  Random Destination
//
//  Created by Edward Salter on 6/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocTVC : UITableViewController

@property (nonatomic) NSArray *venues;

@end
