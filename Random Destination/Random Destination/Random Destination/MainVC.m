//
//  MainVC.m
//  Random Destination
//
//  Created by Edward Salter on 6/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "MainVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import "FSRequest.h"
#import "LocTVC.h"
#import "MapAnnote.h"

@interface MainVC () <CLLocationManagerDelegate>

@end

@implementation MainVC
{
    MKMapView *fsMap;
    CLLocationManager *locManage;
    CLLocation *currentLoc;
    LocTVC *venuesTVC;
}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        locManage = [[CLLocationManager alloc]init];
        locManage.delegate = self;
        [locManage startUpdatingLocation];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    fsMap = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, 320, 320)];
    [self.view addSubview:fsMap];
    venuesTVC = [[LocTVC alloc]init];
    venuesTVC.tableView.frame = CGRectMake(0, 320, 320, [UIScreen mainScreen].bounds.size.height - 320);
    
    [self.view addSubview:venuesTVC.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{

    NSLog(@"%@",locations);
    currentLoc = [locations firstObject];
    
    NSArray *venues = [FSRequest getVenuesWithLat:currentLoc.coordinate.latitude andLong:currentLoc.coordinate.longitude];
    NSLog(@"%@", venues);
    
    [locManage stopUpdatingLocation];
    [self createMapAnnotationWithVenues:venues andLocation:currentLoc.coordinate];
    
    
}

-(void)createMapAnnotationWithVenues:(NSArray *)venues andLocation:(CLLocationCoordinate2D)coordinate
{
    venuesTVC.venues = venues;
    [venuesTVC.tableView reloadData];
    
    
    double minLat = coordinate.latitude,
          minLong = coordinate.longitude,
           maxLat = coordinate.latitude,
          maxLong = coordinate.longitude;
    
    
    
    for (NSDictionary *venue in venues) {
        
        NSDictionary *venueInfo = venue[@"venue"];
        NSDictionary *locationInfo = venueInfo[@"location"];
        
        NSLog(@"%@", locationInfo);
        
        double latitude = [locationInfo[@"lat"] doubleValue];
        double longitude = [locationInfo[@"lng"] doubleValue];
    
        if (latitude < minLat) minLat = latitude;
        if (longitude < minLong) minLong = longitude;
        if (latitude > maxLat) maxLat = latitude;
        if (longitude > maxLong) maxLong = longitude;
        
    
    
        MapAnnote *annotation = [[MapAnnote alloc]init];
        
        [annotation setCoordinate:CLLocationCoordinate2DMake(latitude, longitude)];

        [fsMap addAnnotation:annotation];
        
    }
    
    double centerLat = (maxLat - minLat)/2.0 + minLat;
    double centerLong = (maxLong - minLong)/2.0 + minLong;
    CLLocationCoordinate2D centerCoord = CLLocationCoordinate2DMake(centerLat, centerLong);
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoord, MKCoordinateSpanMake((maxLat - minLat) * 1.2, (maxLong - minLong) * 1.2));

    
    
    [fsMap setRegion:region animated:YES];

}

/*venue: {
id: "4d2f2dff4377224bcf071538"
name: "Mestia | მესტია"
contact: { }
location: {
            lat: 43.043625064722086
            lng: 42.72593427812867
            distance: 22827
            postalCode: "3200"
            cc: "GE"
            state: "Samegrelo and Zemo Svaneti"
            country: "Georgia"
}
*/



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
