//
//  MapAnnote.m
//  Random Destination
//
//  Created by Edward Salter on 6/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "MapAnnote.h"

@implementation MapAnnote

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    _coordinate = newCoordinate;
    //Because property is read only, we have to go to instance property _coordinate
    //to set it
}

@end
