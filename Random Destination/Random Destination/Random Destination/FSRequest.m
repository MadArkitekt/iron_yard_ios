//
//  FSRequest.m
//  Random Destination
//
//  Created by Edward Salter on 6/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "FSRequest.h"

@implementation FSRequest
+(NSArray *)getVenuesWithLat:(double)latitude andLong:(double)longitude
{
    NSArray *venues = @[];
    NSString *locationURL = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/explore?ll=%f,%f&oauth_token=GUW1BG51FLYTEOAMUOADAWBIQLLNVS1KEA44BJ2ICQATXEEK&v=20140605", latitude, longitude];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:locationURL]];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *fsInfo = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    venues = fsInfo[@"response"][@"groups"][0][@"items"];
    
    return venues;
}

@end
