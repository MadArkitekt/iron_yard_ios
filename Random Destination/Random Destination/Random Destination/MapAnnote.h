//
//  MapAnnote.h
//  Random Destination
//
//  Created by Edward Salter on 6/5/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface MapAnnote : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
