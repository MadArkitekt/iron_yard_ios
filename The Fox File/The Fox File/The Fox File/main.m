//
//  main.m
//  The Fox File
//
//  Created by MadArkitekt on 5/18/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TFFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TFFAppDelegate class]));
    }
}
