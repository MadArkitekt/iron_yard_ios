//
//  TFFAppDelegate.h
//  The Fox File
//
//  Created by MadArkitekt on 5/18/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TFFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
