//
//  TLATableViewCell.m
//  Tweeter
//
//  Created by MadArkitekt on 4/23/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TLATableViewCell.h"

@implementation TLATableViewCell
{
    UILabel *textLabel;
    UIImageView *hear;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self) {
            UIImageView *heart = [[UIImageView alloc] initWithFrame:CGRectMake(20, 30, 20, 20)];
            
            heart.image = [UIImage imageNamed:@"heart"];
            
            [self.contentView addSubview:heart];
            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 30, 200, 20)];
            
            
            
            textLabel.text = @"Some text will go here and hopefully it will wrap around the end of the lines on the oddest of days where I stood bloodied but unbowed.";
            textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            textLabel.numberOfLines = 0;
            
            [self.contentView addSubview:textLabel];
            
        }

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
