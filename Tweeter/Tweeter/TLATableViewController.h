//
//  TLATableViewController.h
//  Tweeter
//
//  Created by MadArkitekt on 4/23/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLATableViewController : UITableViewController
@property (nonatomic,getter = isTweetItemsEmpty (BOOL) tweetIsEmpty, readOnly);
@property (nonatomic) NSMutableArray *tweetItems;
- (void)createNewTweet:(NSString *)string;
@end
