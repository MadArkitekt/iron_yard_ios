//
//  TLANavController.m
//  Tweeter
//
//  Created by MadArkitekt on 4/23/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TLANavController.h"
#import "TLATableViewController.h"

@interface TLANavController () <UITextViewDelegate>

@end

@implementation TLANavController
{
    UIButton *addNewItem;
    UIView *blueBox;
    UIView *newForm;
    UIImageView *logo;
    UITextView *captionView;
    TLATableViewController *TVC;
    UITextView *newCaption;
    UIButton *cancel;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        blueBox = [[UIView alloc] initWithFrame:self.navigationBar.frame];
        blueBox.backgroundColor = [UIColor colorWithRed:0.333f green:0.671f blue:0.929f alpha:1.0f];
        [self.view addSubview:blueBox];
        
        newForm = [[UIView alloc] initWithFrame:self.navigationBar.frame];
        newForm.backgroundColor = [UIColor clearColor];
        [blueBox addSubview:newForm];
        
        addNewItem = [[UIButton alloc] initWithFrame:CGRectMake(80, (self.navigationBar.frame.size.height - 30) / 2, 160, 30)];
        addNewItem.backgroundColor = [UIColor whiteColor];
        addNewItem.layer.cornerRadius = 15;
        [addNewItem addTarget:self action:@selector(newItem) forControlEvents:UIControlEventTouchUpInside];
        [addNewItem setTitle:@"Add New" forState:UIControlStateNormal];
        [addNewItem setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [blueBox addSubview:addNewItem];
        NSLog(@"clicked");
        
        float captionBottom = captionView
        
        UIButton *submit
        
        
        float captionBottom = captionView.frame.size.height + captionView.frame.origin.y;
        UIButton submit = [[UIButton alloc] initWithFrame:CGRectMake(40, captionBottom + 20, 80, 40)];
        submit.backgroundColor = UIColor greenColor];
        submit.layer.cornerRadius = submit.frame.size.height / 2;
        [submit setTitle:@"New Tweet" forState:UIControlStateNormal];
        [submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [submit addTarget:self action:@selector(saveTweet) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)]
        cancelButton
        
        [newForm addSubview:cancelButton]    }
    return self;
}
-(void)cancelTweet
{
    [newForm removeFromSuperview];
    [UIView animateWithDuration:0.4 animations:^{
        bluebox
    }
     
     }
     - (void) newItem:(id)sender
     {
         [UIView animatedWithDuration:0.4 animations:^{
             blueBox.frame = self.view.frame;
             addNewItem.alpha = 0.0
         }]
     }
     - (void)saveTweet
     { if ([captionView.text isEqualToString:@""]) return;
         [TVC.tweetItems addobject:@{
                                     @"likes":@0,
                                     @"text":captionView.text
                                     
                                     
                                     }];
         [[TVC.tweetItems insertObject:@{
                                         @"likes":@0,
                                         @"text":captionView.text
                                         } atIndex:0];
          [TVC createnew]
          [TVC.tableView reloadData];
          
          [self cancelTweet];
          }
          - (void)newItem:(id)sender
          [newForm removeFromSuperview];
          
          newForm.frame = self.view.frame;
          UIView animatewithduration
          blueBox.frame = self.navigationBar.frame
          addNewItem.alpha = 1.0;
          - (BOOL)textViewShouldBeginEditing:(UITextView *)textView
          {
              [UIView animateWithDuration:0.4 animations:^{
                  newForm.frame = CGRectMake(0, -216, self.view.frame.size.width, newForm.frame.size.height)
              }];
              return YES;
          }
          - (void)addInitialViewController:(TLATableVC *)
          
          - (void)newItem
          - (void)viewDidLoad
          {
              [super viewDidLoad];
              blueBox = [[UIview alloc]]
              // Do any additional setup after loading the view.
          }
          - (void)addInitialViewController:(TLATableViewController *)viewController
          {
              TVC = viewController;
              [self pushViewController:viewController animated:NO];
              
              if ([TVC isTweetItemsEmpty])
              {
                  [self newItem:nil];
              }
                
          }
          - (void)didReceiveMemoryWarning
          {
              [super didReceiveMemoryWarning];
              // Dispose of any resources that can be recreated.
          }
          - (void)addTableViewController:(TLATableViewController *)viewController
          {
        [
          }
          /*
           #pragma mark - Navigation
           
           // In a storyboard-based application, you will often want to do a little preparation before navigation
           - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
           {
           // Get the new view controller using [segue destinationViewController].
           // Pass the selected object to the new view controller.
           }
           */
          - (BOOL)preferStatusBarHidden;
          {
              return YES;
          }
          
          @end
