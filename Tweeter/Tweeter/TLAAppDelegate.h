//
//  TLAAppDelegate.h
//  Tweeter
//
//  Created by MadArkitekt on 4/23/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
