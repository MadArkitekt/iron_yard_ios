//
//  TDLTableViewController.h
//  ToDoJoMo
//
//  Created by MaddArkitekt on 4/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDLTableViewCell.h"

@interface TDLTableViewController : UITableViewController <UITextFieldDelegate, TDLTableViewCellDelegate>

@end
