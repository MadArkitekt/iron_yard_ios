//
//  TDLTableViewCell.h
//  ToDoJoMo
//
//  Created by MaddArkitekt on 4/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TDLTableViewCellDelegate;

@interface TDLTableViewCell : UITableViewCell


@property (nonatomic, assign) id<TDLTableViewCellDelegate> delegate;


//@property (nonatomic) NSDictionary * listItems;
@property (nonatomic) UIView * bgView;
@property (nonatomic) UIView * strikeThrough;
@property (nonatomic) UIButton * circleButton;
@property (nonatomic) UILabel * nameLabel;
@property (nonatomic) BOOL swiped;


- (void)resetLayout;
// method
- (void)showCircleButtons;
- (void)hideCircleButtons;

- (void)showDeleteButtons;
- (void)hideDeleteButtons;
@end

@protocol TDLTableViewCellDelegate <NSObject>;

- (void)deleteItem:(TDLTableViewCell *)cell;

- (void)setItemPriority:(int)priority withItem:(TDLTableViewCell *)cell;
@end
