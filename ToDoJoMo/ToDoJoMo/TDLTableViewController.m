//
//  TDLTableViewController.m
//  ToDoJoMo
//
//  Created by MaddArkitekt on 4/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TDLTableViewController.h"
#import "TDLTableViewCell.h"
#import "MOVE.h"

@interface TDLTableViewController ()

@end

@implementation TDLTableViewController
{
    UITextField * listItemField;
    NSMutableArray * listItems;
    NSArray * priorityColors;
    
    UIButton * lowButton;
    UIButton * medButton;
    UIButton * highButton;
    
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        priorityColors = @[TAN_COLOR,YELLOW_COLOR,ORANGE_COLOR,RED_COLOR];
        
//        todoItems = [@[] mutableCopy];
        listItems = [@[] mutableCopy];
    
//        [self todoItems];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
       // self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0);
        self.tableView.rowHeight = 50;
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20);
        
        UIView * header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        header.backgroundColor = [UIColor whiteColor];
        

        self.tableView.tableHeaderView = header;
        
        
        UILabel * titleHeader = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 40)];
        titleHeader.text = @"ToDo List";
        titleHeader.textColor = [UIColor lightGrayColor];
        titleHeader.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:30];
        
        
        [header addSubview:titleHeader];
        
        //Shortened the height of the below to 60 and it added in the gap needed
        listItemField = [[UITextField alloc] initWithFrame:CGRectMake(20, 60, 160, 30)];
        listItemField.backgroundColor = [UIColor colorWithWhite:0.0 alpha:.05];
        listItemField.placeholder = @"ToDo Item";
        listItemField.layer.cornerRadius = 6;
        listItemField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 30)];
        listItemField.leftViewMode = UITextFieldViewModeAlways;
        
        listItemField.delegate = self;
        
        [header addSubview:listItemField];
        
        lowButton = [[UIButton alloc] initWithFrame:CGRectMake(195, 60, 30, 30)];
        lowButton.tag = 1;
        lowButton.backgroundColor = YELLOW_COLOR;
        lowButton.layer.cornerRadius = 15;
        [lowButton addTarget:self action:@selector(addNewListItem:)
               forControlEvents:UIControlEventTouchUpInside];
        [header addSubview:lowButton];
        
        medButton = [[UIButton alloc] initWithFrame:CGRectMake(235, 60, 30, 30)];
        medButton.tag = 2;
        medButton.backgroundColor = ORANGE_COLOR;
        medButton.layer.cornerRadius = 15;
        [medButton addTarget:self action:@selector(addNewListItem:)
              forControlEvents:UIControlEventTouchUpInside];
        [header addSubview:medButton];
        
        highButton = [[UIButton alloc] initWithFrame:CGRectMake(275, 60, 30, 30)];
        highButton.tag = 3;
        highButton.backgroundColor = RED_COLOR;
        highButton.layer.cornerRadius= 15;
        [highButton addTarget:self action:@selector(addNewListItem:)
               forControlEvents:UIControlEventTouchUpInside];
        [header addSubview:highButton];
    }
    return self;
}

- (void)deleteItem:(TDLTableViewCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [listItems removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)setItemPriority:(int)priority withItem:(TDLTableViewCell *)cell
{
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    NSDictionary * listItem = listItems[indexPath.row];
    NSDictionary * updateListItem = @{
                                      @"name" : listItem[@"name"],
                                      @"priority" : @(priority),
                                      @"constant" : @(priority)
                                      };
                                                         
    [listItems removeObjectAtIndex:indexPath.row];
    [listItems insertObject:updateListItem atIndex:indexPath.row];
    cell.bgView.backgroundColor = priorityColors[priority];
    [MOVE animateView:cell.bgView properties:@{@"x":@10,@"duration":@0.5}];
    [cell hideCircleButtons];
    cell.swiped = NO;
   NSLog(@"Priotity : %d", priority);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"Returned");
    [textField resignFirstResponder];
    return YES;
}


- (void) addNewListItem:(id)sender
{
    UIButton * button = (UIButton *)sender;
    NSString * name = listItemField.text;
    
    if(![name isEqualToString:@""])
    {   /*
         "constant" is declared upon initialization and therefore maintains the original button.tag of the comment no matter what
         */
        [listItems insertObject:@{@"name" : name, @"priority" : @(button.tag), @"constant" : @(button.tag)} atIndex:0];
        
    }
    
//    NSLog(@"%@", sender);
    [listItemField resignFirstResponder];
    [self.tableView reloadData];
    

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TDLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) cell = [[TDLTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                    reuseIdentifier: @"cell"];
    [cell resetLayout];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.delegate = self;
    
//    cell.toDoInfo = [self getToDoItem:indexPath.row];
    NSDictionary * listItem = listItems[indexPath.row];
    
    cell.bgView.backgroundColor = priorityColors[[listItem[@"priority"] intValue]];
    
    if([listItem[@"priority"] intValue] == 0)
    {
        
        cell.strikeThrough.alpha = 1;
        cell.circleButton.alpha = 0;
    } else {
        cell.strikeThrough.alpha = 0;
        cell.circleButton.alpha = 1;
    };
    
    cell.nameLabel.text = listItem[@"name"];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeCell:)];
    
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [cell addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeCell:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    
    [cell addGestureRecognizer:swipeRight];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get cell from tableView at row
    TDLTableViewCell * cell = (TDLTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
     NSDictionary * listItem = listItems[indexPath.row];
    if(cell.swiped) return;
    // set cell background to done color
    NSDictionary * updateListItem;
    
    if([listItem[@"priority"] intValue])
    {
        cell.bgView.backgroundColor = priorityColors[0];
        cell.strikeThrough.alpha = 1;
        cell.circleButton.alpha = 0;
        updateListItem = @{
                                          @"name" : listItem[@"name"],
                                          @"priority" : @0,
                                          @"constant" : listItem[@"constant"]
                                          };
        
    } else {
    
        cell.bgView.backgroundColor = priorityColors[[listItem[@"constant"] intValue]];
        cell.strikeThrough.alpha = 0;
        cell.circleButton.alpha = 1;
    // create new dictionary with the done priority
        updateListItem = @{
                                      @"name" : listItem[@"name"],
                                      @"priority" : listItem[@"constant"],
                                      @"constant" : listItem[@"constant"]
                                      };
    }
    
    // remove old dictionary for cell
    [listItems removeObjectAtIndex:indexPath.row];
    // add new dictionary for cell
    [listItems insertObject:updateListItem atIndex:indexPath.row];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //"Count" = method returning length of array
    return [listItems count];
}

- (void)swipeCell:(UISwipeGestureRecognizer *)gesture
{
 //   NSLog(@"%lu",gesture.direction);
    TDLTableViewCell * cell = (TDLTableViewCell *)gesture.view;
    
    /* 
     returns 0, 1, 2, 3 using below method to get dictionary from array listItems if 0, then move over a little bit delete it
      */
    NSInteger index = [self.tableView indexPathForCell:cell].row;
    NSDictionary * listItem = listItems[index];
    
    //gesture.direction == left: 2
    //gesture.direction == right: 1
    //gesture.direction == left && priority == 0 : 12
    //gesture.direction == right && priority == 0 : 11
    
    // swopt left or right
    int completed;
//    if([listItem[@"priority"] intValue] == 0)
//    {
//        completed = 1;
//    } else {
//        completed = 0;
//    }
    
    completed = ([listItem[@"priority"] intValue] == 0) ? 10 : 0;
    
    //cell = dictionary
    
    switch (gesture.direction + completed)
    {
        case 1: //right
            [MOVE animateView:cell.bgView properties:@{
                                                @"x" : @10,
                                                @"duration" : @0.5
                                                }];
            cell.swiped = NO;
            [cell hideCircleButtons];
            break;
        case 2: //left
            [MOVE animateView:cell.bgView properties:@{
                                                      @"x" : @-120,
                                                      @"duration" : @0.5
                                                       }];
            cell.swiped = YES;
            
            [cell showCircleButtons];
            break;
        case 11: //right
            [MOVE animateView:cell.bgView properties:@{@"x":@10,@"duration":@0.5}];
            [cell hideDeleteButtons];
            cell.swiped = NO;

            break;
        case 12: //left
            [MOVE animateView:cell.bgView properties:@{@"x":@-40,@"duration":@0.5}];
            [cell showDeleteButtons];
            cell.swiped = YES;

        default:
            break;
    }
}

- (BOOL)prefersStatusBarHidden {return YES;}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
/*- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

*/
/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
