//
//  TTTTwoVC.m
//  Two-Three Tap
//
//  Created by MadArkitekt on 5/7/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//////////////////////////////////////////////////////////
//                      BLUE                                 //
//////////////////////////////////////////////////////////

#import "TTTTwoVC.h"
#import "TTTStats.h"
@interface TTTTwoVC ()

@end

@implementation TTTTwoVC
{
  
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UIView *blueHalf = 
        self.view.frame = CGRectMake(0, (SCREEN_HEIGHT / 2), SCREEN_WIDTH, (SCREEN_HEIGHT / 2));
        self.view.backgroundColor = [UIColor blueColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void) touchesMoved:( NSSet *) touches withEvent:( UIEvent *) event
{ 
    for (touches in self.view.frame) {
        <#statements#>
    } == [UIColor blueColor])
    {
        [[TTTStats tapStats]addPointBlue];
    } else {
        [[TTTStats tapStats]addPointRed];
    }
}

@end
