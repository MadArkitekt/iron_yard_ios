//
//  TTTAppDelegate.h
//  Two-Three Tap
//
//  Created by MadArkitekt on 5/7/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
