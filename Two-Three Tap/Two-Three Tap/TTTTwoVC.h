//
//  TTTTwoVC.h
//  Two-Three Tap
//
//  Created by MadArkitekt on 5/7/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TTTTwoVCDelegate;


@interface TTTTwoVC : UIViewController

-(void)willRefreshBlue;
-(void)willRefreshRed;



@end
