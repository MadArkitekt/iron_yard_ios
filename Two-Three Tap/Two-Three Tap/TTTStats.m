//
//  TTTStats.m
//  Two-Three Tap
//
//  Created by MadArkitekt on 5/7/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TTTStats.h"

@implementation TTTStats
{
    NSInteger *redScore;
    NSInteger *blueScore;
}
+ (TTTStats *) tapStats
{
    static  dispatch_once_t create;
    static TTTStats *singleton;
    dispatch_once(&create, ^{
    singleton = [[TTTStats alloc] init];

    });
    return singleton;
}
-(void)addPointBlue
{
    redScore += 1;
    NSLog(@"Touches Red: %d", (NSInteger)redScore);
}

-(void)addPointRed
{
    blueScore += 1;
    NSLog(@"Touches Blue: %d", (NSInteger)blueScore);
}
-(NSInteger *)returnPointsBlue
{
return blueScore;
}
-(NSInteger *)returnPointsRed;
{
    return redScore;
}
@end
