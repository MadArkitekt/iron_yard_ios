//
//  TTTOneVC.m
//  Two-Three Tap
//
//  Created by MadArkitekt on 5/7/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TTTOneVC.h"
#import "TTTTwoVC.h"
@interface TTTOneVC () <TTTTwoVCDelegate>
@end
@implementation TTTOneVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        TTTOneVC *blue = [[TTTOneVC alloc] initWithNibName:nil bundle:nil];
        blue.view.backgroundColor = [UIColor blueColor];
        
        TTTOneVC *red = [[TTTOneVC alloc] initWithNibName:nil bundle:nil];
        red.view.backgroundColor = [UIColor redColor];
       red.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, (SCREEN_HEIGHT / 2));
        [self.view addSubview:blue.view];
        [self.view addSubview:red.view];

           }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-
(NSInteger *)returnPointsBlue;
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
