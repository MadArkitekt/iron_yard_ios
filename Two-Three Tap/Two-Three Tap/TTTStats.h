//
//  TTTStats.h
//  Two-Three Tap
//
//  Created by MadArkitekt on 5/7/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTTStats : NSObject
+ (TTTStats *) tapStats;
//@property NSInteger *redScore;
//@property NSInteger *blueScore;
-(void)addPointBlue;
-(void)addPointRed;
-(NSInteger *)returnPointsRed;
-(NSInteger *)returnPointsBlue;
@end
