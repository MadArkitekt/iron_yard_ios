//
//  TGASingleton.m
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TGASingleton.h"

@implementation TGASingleton
{ int blueScore;
    int redScore;
}
+ (TGASingleton *) mainData;
{

    static dispatch_once_t create;

    static TGASingleton *singleton;
    
    dispatch_once(& create,^{
        singleton = [[TGASingleton alloc] init];
        
        
    });
    return singleton;
}

- (int)blueTotal
{
    blueScore += 1;
    NSLog(@"Touches Blue: %d", blueScore);

    return blueScore;
}

- (int)redTotal
    {      redScore += 1;
    NSLog(@"Touches Red: %d", redScore);
    return redScore;
}

@end
