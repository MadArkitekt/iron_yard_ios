//
//  TGASubclass.m
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TGASubclass.h"

@implementation TGASubclass
-(void)viewDidLoad
{
    self.view.frame = CGRectMake(0, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT / 2);
    self.view.backgroundColor = [UIColor redColor];
}
@end
