//
//  TGAAppDelegate.h
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
