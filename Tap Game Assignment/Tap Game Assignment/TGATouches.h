//
//  TGATouches.h
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGAScoreDelegate;

@interface TGATouches : UIViewController

@property (nonatomic, assign) id<TGAScoreDelegate> delegate;


@end

@protocol TGAScoreDelegate <NSObject>

@optional
-(void)refreshBlue;
-(void)refreshRed;

@end
