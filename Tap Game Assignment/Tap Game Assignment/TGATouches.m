//
//  TGATouches.m
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TGATouches.h"
#import "TGASingleton.h"
@interface TGATouches ()

@end

@implementation TGATouches
{
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, (SCREEN_HEIGHT / 2));
    self.view.backgroundColor = [UIColor blueColor];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        if (self.view.backgroundColor == [UIColor blueColor])
        {    NSLog(@"disconnect");

            [_delegate refreshBlue];
        } else {
            NSLog(@"disconnect2");

            [self.delegate refreshRed];
            }
}
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
