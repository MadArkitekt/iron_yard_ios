//
//  TGASingleton.h
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGASingleton : NSObject
+ (TGASingleton *) mainData;

- (int)redTotal;
- (int)blueTotal;

@end
