//
//  main.m
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TGAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TGAAppDelegate class]));
    }
}
