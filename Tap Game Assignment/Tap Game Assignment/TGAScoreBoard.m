//
//  TGAScoreBoard.m
//  Tap Game Assignment
//
//  Created by MadArkitekt on 5/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "TGAScoreBoard.h"
#import "TGASingleton.h"
#import "TGATouches.h"
#import "TGASubclass.h"
@interface TGAScoreBoard () <TGAScoreDelegate>

@end

@implementation TGAScoreBoard
{
    TGATouches *blueSide;
    TGASubclass *redSide;
    UILabel *redLabel;
    UILabel *blueLabel;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        blueSide = [[TGATouches alloc] initWithNibName:nil bundle:nil];
        redSide = [[TGASubclass alloc] initWithNibName:nil bundle:nil];
        blueSide.delegate = self;
        redSide.delegate = self;

        
        [self.view addSubview:blueSide.view];
        [self.view addSubview:redSide.view];
        
       
    }
    return self;
}

- (void)viewDidLoad
{    [super viewDidLoad];

    blueLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH * 0.25, SCREEN_HEIGHT * 0.2, SCREEN_WIDTH * 0.5, SCREEN_HEIGHT * 0.12)];
    blueLabel.textColor = [UIColor blackColor];
    blueLabel.textAlignment = NSTextAlignmentCenter;
    blueLabel.backgroundColor = [UIColor grayColor];
    
    [blueSide.view addSubview:blueLabel];
    
//    NSLog(@"show ");
    redLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH * 0.25, SCREEN_HEIGHT * 0.2, SCREEN_WIDTH * 0.5, SCREEN_HEIGHT * 0.12)];
    redLabel.textColor = [UIColor blackColor];
    redLabel.textAlignment = NSTextAlignmentCenter;
    redLabel.backgroundColor = [UIColor grayColor];
    [redSide.view addSubview:redLabel];
//    NSLog(@"show something");

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)refreshBlue
{
 //   NSLog(@"hi");
    int blue;
    blue = [[TGASingleton mainData] blueTotal];
    blueLabel.text = [NSString stringWithFormat:@"Blue total: %d", blue];
    [blueLabel setNeedsDisplay];
}

- (void)refreshRed
{   int red;
    red = [[TGASingleton mainData] redTotal];
    
//   NSLog(@"hi2");
    redLabel.text = [NSString stringWithFormat:@"Red total: %d", red];
    [redLabel setNeedsDisplay];
}


@end
