# 10 App Review Regarding Aesthetics and Functionality 
###-Courtesy of _The IronhEaD Initiative_ 

------- 

##REVIEW FORMAT: 
>1. Description of Navigation Flow 
2. Critique of Button Visibility and Accessibility 
3. Review of Design and Layout 
4. Functionality Tests: Deliberator Incorrect Field Submissions & Interactivity Exploration 
5. Presence or Lack of In App purchases 


##*WIKIPANION+* 
>###*Wikipedia.org Search Engine* 
>>__1. Navigation Flow:__ A minimalistic approach affords an uncluttered view of the article's text. Additionally, each button performs a single action, maybe two, keeping navigation intuitive and easily mastered. While navigating from the wiki to the included dictionary, a simple horizontal screen rotation adds a tasteful flourish. Other page/menu transformations involve pages sliding vertically, covering and uncovering the existing text.   
__2. Buttons:__ Other than the blue, hyperlinked keywords found throughout the articles, buttons are kept spartan: simple black text and monochromatic, stenciled buttons.   
__3. Icon:__ Extremely well executed, with a stylized white "W" superimposed upon a soft, pastel background. Tactful shadowing coupled with the icon's rounded edges instill a sense of substance and weight.   
__4. Functionality Tests:__ I have relied on this app for years, and never experienced any failure to function properly. The app installs a miniature version of Wikipedia.org's library, making up for any lack of information by offering a full search of Wikipedia.org via a button.   
__5. In App Offerings:__ None.   

##*UNITS PLUS* 
>###*Unit Conversion Calculator* 
>>__1. Navigation Flow:__ Conversions are subdivided according to subject matter. App initialization is accompanied by a swift zooming into the icon, which becomes the app's homescreen during said transition. Units Plus's basic homescreen offeres a grid of icons arranged 4x3. Calculator selection results in a screen wipe from the homescreen to the calculator itself. Each calculator presents two vertically rotating wheels for selecting which units the user wishes to convert between. The wheels "snap" to the catagories.   
__2. Buttons:__ Buttons are slightly shadowed, affording a tangible aesthetic. Each button tints to a darker shade upon interaction and is extremely quick to reset. The vertically rotating menues latches into a touched state, allowing the finger to swipe over other buttons while still rotating the wheel. Both wheels exhibit a "magnetic" snapping to the nearest value, making navigation a breeze.    
__3. Icon:__ Icons are crisp, spaced well, and vibrant. The colors really pop, without being overbearing, and all icons contain a secondary image which appears extruded upward.    
__4. Functionality Tests:__ Everything functions as expected, and calculation lag is unnoticeable.     
__5. In App Offerings:__ None.    

##*LUMOSITY* 
>###*Neuroplasticity Exercersises* 
>>__1. Navigation Flow:__ A very linear flow with few choices. Users are presented with a very spartan version of the online site, maintaining. The program, as well as the website, are both extremely linear, with the transitions between pages being varied, yet repeated for each game played as part of its prescribed regime.    
__2. Buttons:__ The flat, yet juxtaposed colors attract attention without causing distraction, and are large enough to easily interact with.    
__3. Icon:__ Another, solidly 2-dimensional icon graces this application, and is superbly designed, a minimalistic design without being spartan.    
__4. Functionality Tests:__ The app functions flawlessly.   
__5. In App Offerings:__ None.    

##*PROPELLERHEAD FIGURE* 
>###*Synthesizer/Sampler* 
>>__1. Navigation Flow:__ Tabs at the top of the page allow switching between instrument types, wiping pages horizontally. Selecting from a variety of instruments involves a simple swipe left or right (the instrument exiting view fades into the background), which changes the possible "Tweaks" accordingly. Interacting with tabs at the bottom of the app, extends said tabs upward while moving changing the main views view wiping screens vertically, while the "System" tab wipes from right-to-left when activated.    
__2. Buttons:__ Pull tabs and transitioning emphasis from, for lack of a better word, "button frames" dominate the app's interactivity. When activated, tabs extend toward the center of the screen. Each instrument has atleast a rhythm dial, controlled by touching the button and dragging the finger vertically, thereby changing the beats per bar. The bass and lead, pitched instruments, have "Range" and "Scale" sliders, both represented as circular objects whose values, represented by changes in physical appearance, are manipulated by sliding vertically (with values increases the futher upware said swipe travels).    
__3. Icon:__ "Simple, crisp, and tasteful" describes the the icon and app as a whole. Unlike the apps I've previously reviewed, Figure's icon has no shading, and is solidly rooted int he second dimension. The text is very plain, as is the app as a whole, focusing on usability far more than a flashy UIX.   
__4. Functionality Tests:__ The controls are intuitive and responsive with no bugs nor need for a manual.    
__5. In App Offerings:__ None.    

##*CHEAT DOCS* 
>###Collection of Computer Programming Shortcuts 
>>__1. Navigation Flow:__ Chosing from the primary catagories wipes the homescreen left while choosing from the second menu wipes downward revealing keyboard and syntax shortcuts related to your choices. Transitions and animation appear extremely generic and qucikly coded using some sort of template or app-generator.    
__2. Buttons:__ Buttons are tint dark grey during interaction or emulate depression, depending on their placement. The first set of buttons is an interactive table while the second are older style.    
__3. Icon:__ The icon has depth and is tactfully ornate, with a dog-eared page and attention to detail. The icon is easily the best designed faced of the app.    
__4. Functionality Tests:__ Both Java buttons are broken and result point toward the last active page.    
__5. In App Offerings:__ None.    

##*DROPBOX* 
>###*Cloud Storage* 
>>__1. Navigation Flow:__ Dropbox snaps to new views rather than transition between screens. The speed with which it processes input is unparalleled by other apps in this review.    
__2. Buttons:__ The entire user interface presents a refreshingly clean design with zero clutter and the buttons are translucent, but still provide enough contrast, with regard to the background, so as to be easily recognized and manipulated. *The one transition noted by the author is a screen wipe left to right when returning to the main file repository after viewing a selected file.    
__3. Icon:__ Dropbox's icon is nothing more than a flattened version of its logo, an open blue box on a white background, meshing well with the rest of the GUI.    
__4. Functionality Tests:__ Blazing speed: enough said.    
__5. In App Offerings:__ A "Pro Upgrade" to 100 Gb of storage for $99.99.    

##*Producteev* 
>###*Scheduling and Checklist App 
>>__1. Navigation Flow:__ Navigation is smoothe and the effort creating smooth transitions between views (I'm assumming because juggling activities can be stressful) presents a calm environment, putting the mind at ease. As with the other apps I've reviewed, view changes between more interactive portions of the app are far crisper than screen changes leading opening an app containing literary fiction. Vertical scrolling through the list of daily to-do's is smoothe and entering new tasks, plus color coating them according to importance, is a breeze in the predominantly blue pastel environment.    
__2. Buttons:__ The buttons are very austere, and focus mainly on functionality, so as not to distract the users from the task of scheduling their busy day.    
__3. Icon:__ The icon doesn't appear to be much at first: a small kitchen tile, perhaps. But the "bull" graphic superimposed on the tile is quite eyecatching with several shades of blue coming together and mixing at symmetric points along the figure's anatomy, creating new shades.   
__4. Functionality Tests:__ I have experienced zero problems with this app.    
__5. In App Offerings:__ None.    

##*iMaschine* 
>###*Drum Machine* 
>>__1. Navigation Flow:__ Transitions between larger portions of the app which completely change the view are executed instantly, while interactions leading less significant (the users with most assuredly spend less time interacting in said views) do exhibit varying transitions including screen wipes from all angles and shutter effects.    
__2. Buttons:__ Buttons are shadowed for a 3D effect, though the effect is much more subtle than a more game or graphic design app. Buttons change become darker or brighter with interaction, depending on their purpose. Tabs become lighter and remain as such until another tab is selected, while the drum pads flash brighter then become darker during "reset".    
__3. Icon:__ Maschine's icon is a shrunken version of its official logo in white on a dark orange background. The icon appears to bulge outward from the page thanks to careful shadowing.    
__4. Functionality Tests:__ I have personally experienced problems with failure to complete to transactions and lost recorded sessions to the app crashing.    
__5. In App Offerings:__ Additional sound kits are $.99 and typically include 3-4 new "banks" of sound, such as 4 drum kits, or two synthesizers and 2 drum kits.    

##*Beats Music* 
>###*Music Player and Purchaser*
>>__1. Navigation Flow:__ Screen slide left and right, bouncing in the same fashing as iPhone's tableview does. Additionally, screen dedicated to suggesting music the user should enjoy, based on their oprevious selections/purchases, scroll vertically, revealing decades of artists and music. Menus appear recessed into the side of the app as final swipes to the left and right slide the manipulated page 3/4 out of frame, and reveal either a user menue (on the left) or a menue traditional genre and artist based menue, for the less expeditious.    
__2. Buttons:__ Buttons exhibit on superficial changes, such as differentiation in shading, but do not exhibit in depth-altering effects. The majority of the app is manipulated by swiping either left or right.    
__3. Icon:__ The iron is that of the company, Beats, yet with a touch more shadowing to give a very slight 3-D appearance.
__4. Functionality Tests:__ I have encounted app crashing when downloading multiple songs.    
__5. In App Offerings:__ An in-game currency can be purchases for legal tender.    

##*iOctocat* 
>###*GitHub Account Manager*
>>__1. Navigation Flow:__ Panes and views wipe horizontally with and the overall flow of the app is extremely relaxed and child's play to navigate.    
__2. Buttons:__ iOctocat's buttons are by far the worst I have ever interacted with. Button consist of a simple text printo onto a background, without boarders and having no changes in appearance when manipulated. I find the iPhone 5s extremely accurate regarding its touch technology, but still hesistate when tapping it's screen in iOctocat for fear loosing vital GitHub information. (Additionally, I spent a good 10-15minutes searching for the button that would link to my personal repositories and almost gave up on the app for another exhibiting clearer buttons. HOWEVER, I am completely satisfied with the choice I made.)   
__3. Icon:__ I find the name and icon combination from iOctocat a subliminal hammer, beating memory of the app into the user's memory. The picture itself is of a forking line while the name refers to GitHub's logo of a black cat.    
__4. Functionality Tests:__ Though GitHub is difficult for me to manage at times, I've never used a git before, especially one with the extensive features GitHub exhibits.   
__5. In App Offerings:__ None.   