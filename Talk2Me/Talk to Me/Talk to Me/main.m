//
//  main.m
//  Talk to Me
//
//  Created by MadArkitekt on 6/2/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTMAppDelegate class]));
    }
}
