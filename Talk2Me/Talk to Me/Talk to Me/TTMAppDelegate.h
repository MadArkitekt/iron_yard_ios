//
//  TTMAppDelegate.h
//  Talk to Me
//
//  Created by MadArkitekt on 6/2/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
