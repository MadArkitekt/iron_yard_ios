//
//  main.m
//  Teetit
//
//  Created by MadArkitekt on 4/21/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TIAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TIAAppDelegate class]));
    }
}
