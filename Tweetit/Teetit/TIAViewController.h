//
//  TIAViewController.h
//  Teetit
//
//  Created by MadArkitekt on 4/21/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>
#define NAME_KEY tweet[@"name"];

@interface TIAViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;
@property (nonatomic)   NSDictionary * tweet;
@end
