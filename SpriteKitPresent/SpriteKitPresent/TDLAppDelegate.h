//
//  TDLAppDelegate.h
//  SpriteKitPresent
//
//  Created by MaddArkitekt on 4/8/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
