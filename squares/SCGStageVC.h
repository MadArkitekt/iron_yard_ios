//
//  SCGstageVC.h
//  squares
//
//  Created by MaddArkitekt on 4/11/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCGCircle.h"

@interface SCGStageVC : UIViewController <SCGCircleDelegate>

@end
