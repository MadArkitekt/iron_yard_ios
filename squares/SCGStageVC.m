//
//  SCGstageVC.m
//  squares
//
//  Created by MaddArkitekt on 4/11/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "SCGStageVC.h"
#import "SCGSquare.h"
#import "SCGHomeView.h"


//@interface SCGStageVC ()
// Would declare private methods and properties in this area
//@end

@implementation SCGStageVC
{
    int gameSize;
    int playerTurn;
    
    NSArray * playerColors;
    NSMutableDictionary * tappedDots;
    NSMutableDictionary * allSquares;
    NSString * topLeftDot;
    NSString * topRightDot;
    NSString * bottomLeftDot;
    NSString * bottomRightDot;
    UIButton * startButton;
    UIButton * fourButton;
    UIButton * eightButton;
    UIButton * twelveButton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
        playerColors = @[BLUE_COLOR,ORANGE_COLOR];
        playerTurn = 0;
        
        tappedDots = [@{} mutableCopy];
        allSquares = [@{} mutableCopy];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    fourButton = [[UIButton alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT*.15, 80, 40)];
    [fourButton setTitle:@"4" forState:UIControlStateNormal];
    fourButton.tag = 4;
    fourButton.layer.cornerRadius = 20;
    fourButton.backgroundColor = YELLOW_COLOR;


    // Need to reset size of gameboard to 4 with ^
    
    eightButton = [[UIButton alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT*.15, 80, 40)];
    [eightButton setTitle:@"8" forState:UIControlStateNormal];
    eightButton.tag = 8;
    eightButton.layer.cornerRadius = 20;
    eightButton.backgroundColor = ORANGE_COLOR;
    

    // Need to reset size of gameboard to 8 with ^

    twelveButton = [[UIButton alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT*.15, 80, 40)];
    [twelveButton setTitle:@"12" forState:UIControlStateNormal];
    twelveButton.tag = 12;
    twelveButton.layer.cornerRadius= 20;
    twelveButton.backgroundColor = RED_COLOR;
    // Need to reset size of gameboard to 12 with ^

    [startButton addTarget:self action:@selector(resetGame) forControlEvents:UIControlEventTouchUpInside];

    startButton = [[UIButton alloc] initWithFrame:CGRectMake(80, SCREEN_HEIGHT*.8, 160, 40)];
    [startButton setTitle:@"Let's GET IT ON!" forState:UIControlStateNormal];
    startButton.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:startButton];

    //[lowButton addTarget:self action:@selector(addNewListItem:)
//forControlEvents:UIControlEventTouchUpInside];
// ^= first portion of code for passing tag value of button to gameBoard
    // (id) will be in the new one
}
//- (void)pressPriorityButton:(id)sender
//{
//    UIButton * button = (UIButton *)sender;
 [self.delegate setItemPriority:(int)button.tag withItem:self];
//    
- (void)createButtons
{
    
    eightButton = [[UIButton alloc] initWithFrame:CGRectMake(235, 5, 30, 30)];
    [eightButton addTarget:self action:@selector(pressPriorityButton:)forControlEvents:UIControlEventTouchUpInside];
  //  [self.contentView addSubview:eightButton];
    
    twelveButton = [[UIButton alloc] initWithFrame:CGRectMake(275, 5, 30, 30)];
    [twelveButton addTarget:self action:@selector(pressPriorityButton:)
         forControlEvents:UIControlEventTouchUpInside];
  //  [self.contentView addSubview:twelveButton ];
    
}


- (void)pressGameBoardSelector:(id)sender
{
    UIButton * button = (UIButton *)sender;
    [self.delegate setItemPriority:(int)button.tag withItem:self];

}
- (void) gameBoard
    {
        [startButton removeFromSuperview];
        
    gameSize = 4;
    
    float circleWidth = SCREEN_WIDTH / gameSize;
    float squareWidth = circleWidth / 1.25;
    float squareOffset = circleWidth - (squareWidth / 2 );
    // keeps drawn square from touching circles
    // create squares
    for (int sRow = 0; sRow < gameSize - 1; sRow++)
    {
        for (int sCol = 0; sCol < gameSize - 1; sCol++)
        {
            float squareX = squareOffset + (circleWidth * sCol);
            float squareY = squareOffset + (circleWidth * sRow) + ((SCREEN_HEIGHT - SCREEN_WIDTH) / 2);
            
            SCGSquare * square = [[SCGSquare alloc] initWithFrame:CGRectMake(squareX, squareY, squareWidth, squareWidth)];
            
            square.backgroundColor = [UIColor clearColor];
            square.layer.cornerRadius = 10;

            NSString * key = [NSString stringWithFormat:@"c%dr%d",sCol,sRow]; // 0,1 c0r1
            
            allSquares[key] = square;
            
            [self.view addSubview:square];
        }
    }
    
    // create circles
    for (int row = 0; row < gameSize; row++) {
        for (int col = 0; col < gameSize; col++) {
            float circleX = circleWidth * col;
            float circleY = (circleWidth * row) + ((SCREEN_HEIGHT - SCREEN_WIDTH) / 2);
            
            SCGCircle * circle = [[SCGCircle alloc]initWithFrame:CGRectMake(circleX, circleY, circleWidth, circleWidth)];
            
            circle.position = (CGPoint){col,row};
            circle.delegate = self;
            
            NSString * key = [NSString stringWithFormat:@"c%dr%d", col,row]; // 0,1 c0r1
            
            tappedDots[key] = @2; // dynamically filling dictionary at all possible spots with default of: 2
            
            [self.view addSubview:circle];
        }
    }
}

- (UIColor *)circleTappeWithPosition:(CGPoint)position
{
    
    NSString * key = [NSString stringWithFormat:@"c%dr%d", (int)position.x, (int)position.y];
    // set player num to value in tappedDots
    tappedDots[key] = @(playerTurn);
    
    // check for square
    [self checkForSquareAroundPosition:position];
    
    UIColor * currentColor = playerColors[playerTurn];
    
    playerTurn = (playerTurn) ? 0 : 1;
    return currentColor;

}

- (void)checkForSquareAroundPosition:(CGPoint)position
{
    
    int pX = position.x;
    int pY = position.y;
    
    BOOL above = (pY > 0);               //if the position y > 0, there is something above
    BOOL below = (pY < gameSize -1);     //if the position y < 3, there is something below
    BOOL left = (pX > 0);                //if the position x > 0, there is something left
    BOOL right = (pX < gameSize -1);     //if the position x < 3, there is something right

    if (above && left)
    {
        [self checkSquareForQuadrantAtPosition:(CGPoint){pX-1,pY-1}];
    }
    if (above && right)
    {
        [self checkSquareForQuadrantAtPosition:(CGPoint){pX,pY-1}];
    }
    if (below && left)
    {
        [self checkSquareForQuadrantAtPosition:(CGPoint){pX-1,pY}];
    }
    if (below && right)
    {
        [self checkSquareForQuadrantAtPosition:(CGPoint){pX,pY}];
    }
}


    
- (void)checkSquareForQuadrantAtPosition:(CGPoint)position
{
    int pX = position.x;
    int pY = position.y;
    
    for (UIColor * color in playerColors)
    {
        int player = [playerColors indexOfObject:color];
        
        topLeftDot = [NSString stringWithFormat:@"c%dr%d", pX,pY];
        topRightDot = [NSString stringWithFormat:@"c%dr%d", pX+1,pY];
        bottomLeftDot = [NSString stringWithFormat:@"c%dr%d", pX,pY+1];
        bottomRightDot = [NSString stringWithFormat:@"c%dr%d", pX+1,pY+1];
        // checks if top left and top right are the same
        BOOL topDotsSame = ([tappedDots[topLeftDot] isEqualToValue:tappedDots[topRightDot]]);
        // checks if bottom left and bottom right are the same
        BOOL bottomDotsSame = ([tappedDots[bottomLeftDot] isEqualToValue:tappedDots[bottomRightDot]]);
        // checks if top left and bottom left are the same
        BOOL leftDotsSame = ([tappedDots[topLeftDot] isEqualToValue:tappedDots[bottomRightDot]]);
        
        //if top, left, & bottom dots the same as player... thenthey own the square
        if (topDotsSame && bottomDotsSame && leftDotsSame && [tappedDots[topLeftDot] isEqual:@(player)])
        {
            SCGSquare * currentSquare = allSquares[topLeftDot];
            
            currentSquare.backgroundColor = color;
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)prefersStatusBarHidden { return YES; }

@end
