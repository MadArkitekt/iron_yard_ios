//
//  SCGCircle.h
//  squares
//
//  Created by MaddArkitekt on 4/11/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCGCircleDelegate;

@interface SCGCircle : UIView

@property (nonatomic, assign) id<SCGCircleDelegate> delegate;

@property (nonatomic) CGPoint position;

@end

@protocol SCGCircleDelegate <NSObject>


/* This is telling the view controller to return the color that the circle needs to change to for that touch even during
   that turn
 */
- (UIColor *)circleTappeWithPosition:(CGPoint)position;

@end