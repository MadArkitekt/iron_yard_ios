//
//  BBGSingHiScore.h
//  Data Now App
//
//  Created by MadArkitekt on 5/6/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BBGSingHiScore : NSObject

+ (BBGSingHiScore *)mainData;
@property (nonatomic, readonly) NSInteger topScore;
@property (nonatomic) NSInteger currentScore;
- (NSArray *)gameScores;
@end
