//
//  BBGLevelController.m
//  MetalRain
//
//  Created by MaddArkitekt on 4/17/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "BBGLevelController.h"
#import "MOVE.h"
#import "BBGViewController.h"
#import "AVFoundation/AVFoundation.h"
#import "BBGSingHiScore.h"



@interface BBGLevelController () <UICollisionBehaviorDelegate, AVAudioPlayerDelegate>

@property (nonatomic) NSMutableArray *players;
@property (nonatomic) UICollisionBehavior *collider;
@property (nonatomic) UIView *paddle;
@property (nonatomic) NSMutableArray *balls;
@property (nonatomic) NSMutableArray *bricks;
// Dynamic animator that acts as middle man who relays collision events
@property (nonatomic) UIDynamicAnimator *animator;
// Starts our ball off in one direction (get's it going)
@property (nonatomic) UIPushBehavior *pusher;
// handles collisions
// item behavior properties
// allowing for different interactions to happen based on their properties
@property (nonatomic) UIDynamicItemBehavior *paddleDynamicProperties;
@property (nonatomic) UIDynamicItemBehavior *ballsDynamicProperties;
@property (nonatomic) UIDynamicItemBehavior *bricksDynamicProperties;
// item attachment - attaches items to spots on the screen
@property (nonatomic) UIAttachmentBehavior *attacher;

@end

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation BBGLevelController
{
    int lives;
    int points;
    float paddleWidth;
    float pointTotal;
    NSString *brickBroke;
    UILabel *scoreBoard;
    UILabel *score;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.players = [@[]mutableCopy];
        self.bricks = [@[]mutableCopy];
        self.balls = [@[]mutableCopy];
        paddleWidth = 100;
        //self.view.backgroundColor = [UIColor colorWithWhite:0.15 alpha:1.0];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapScreen:)];
        [self.view addGestureRecognizer:tap];
        
    }
    return self;
}
- (void)playSoundWithName:(NSString *)soundName
{
    NSString *file = [[NSBundle mainBundle] pathForResource:soundName ofType:@"wav"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:file];
    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    player.delegate = self;
    [self.players addObject:player];
    [player play];
    
    //    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    //    self.player.numberOfLoops = 0;
    //    [self.player play];
}
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag  //will remove players to prevent crash
{
    [self.players removeObjectIdenticalTo:player];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    lives = 3;
    [self.delegate respondsToSelector:@selector(addPoints:)];
}


- (void)resetLevel
{
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    [BBGSingHiScore mainData].currentScore = 0;
    [self createPaddle];
    
    self.collider = [[UICollisionBehavior alloc] initWithItems:[self allItems]];
    self.collider.collisionDelegate = self;
    
    self.collider.collisionMode = UICollisionBehaviorModeEverything;
    
    int w = self.view.frame.size.width;
    int h = self.view.frame.size.height;
    
    [self.collider addBoundaryWithIdentifier:@"ceiling" fromPoint:CGPointMake(0, 0) toPoint:CGPointMake(w, 0)];
    [self.collider addBoundaryWithIdentifier:@"leftWall" fromPoint:CGPointMake(0, 0) toPoint:CGPointMake(0, h)];
    [self.collider addBoundaryWithIdentifier:@"rightWall" fromPoint:CGPointMake(w, 0) toPoint:CGPointMake(w, h)];
    [self.collider addBoundaryWithIdentifier:@"floor" fromPoint:CGPointMake(0, h + 10) toPoint:CGPointMake(w, h + 10)];
    [self.animator addBehavior:self.collider];
    
    
    [self createBricks];
    [self createBall];
    
    // assigns boundary's bounds and allows
    //  self.collider.translatesReferenceBoundsIntoBoundary = YES;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    self.ballsDynamicProperties = [self createPropertiesForItems:self.balls];
    
    self.bricksDynamicProperties = [self createPropertiesForItems:self.bricks];
    
    self.paddleDynamicProperties = [self createPropertiesForItems:@[self.paddle]];
    self.paddleDynamicProperties.density = 100000;
    
    self.bricksDynamicProperties.density = 100000;
    
    self.ballsDynamicProperties.elasticity = 1;
    
    self.ballsDynamicProperties.resistance = 0.0;
    
    //    self.ballsDynamicProperties.allowsRotation = YES;
}

- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item1 withItem:(id<UIDynamicItem>)item2 atPoint:(CGPoint)p
{
    if([item1 isEqual:self.paddle] || [item2 isEqual:self.paddle])
    {
        NSLog(@"Paddle strike");
        [self playSoundWithName:@"sound2"];
    }
    UIView *tempBrick;
    for (UIView *brick in self.bricks)
    {
        if ([item1 isEqual:brick] || [item2 isEqual:brick])
        {
            tempBrick = brick;
        }
        if((brick.alpha = 0.5))
        {
            [brick removeFromSuperview];
            [self.collider removeItem:brick];
            [self brokeLabelWithFrame:brick];
            [BBGSingHiScore mainData].currentScore += brick.tag;
        } else {
            brick.alpha = 0.5;
        }
        if (tempBrick != nil)
        {  NSLog(@"Test");
            [self playSoundWithName:@"sound1"];
            [self.bricks removeObjectIdenticalTo:tempBrick];
        }
    }
}




//         if([self.delegate respondsToSelector:@selector([BBGSingHiScore mainData].currentScore:(int)points])

//         points += brick.tag;
//        NSInteger currentScore = [BBGSingHiScore mainData].currentScore;

//scoreBoard = nil;
// [self.delegate addPoints:points];








- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p
{
    for (UIView *ball in self.balls)
    {
        if ([(NSString*)identifier isEqualToString:@"floor"] && [item isEqual:ball])
        {
            NSLog(@"lost a life");
            lives -= 1;
            
            [ball removeFromSuperview];
            [self.collider removeItem:ball];
            
            switch (lives) {
                case (0):
                    [self.delegate respondsToSelector:@selector(gameDone)];
                    [self.delegate gameDone];
                    break;
                    
                    //PROBLEM: Inside cases themselves, NSLog is printing, so case is evaluating, but collision does not occur
                default:
                    [self createBall];
                    break;
            }
        }
    }
}
//   [ball removeFromSuperview];


//BELOW: Goes and finds delegate, and asks: "Have you implemented it in your .m file
//"If you do not, then I won't talk to you about it"
//*****IF YOU DO OPTIONAL METHODS, YOU NEED TO INCLUDE LINE self.delegate
//
//            [self createBall];
//            [self.collider addItem:ball];
//        } else {
//        [self.collider removeItem:ball];
//        [self.delegate respondsToSelector:@selector(gameDone)];
//        [self.delegate gameDone];
//  [self resetLevel];
//   [self.delegate lifeCount:(lives - 1)];



- (void)brokeLabelWithFrame:(UIView *)brick
{
    UILabel * label = [[UILabel alloc] initWithFrame:brick.frame];
    label.text = [NSString stringWithFormat:@"+%d",(int)brick.tag];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    //    [self.view addSubview:label];
    
    [UIView animateWithDuration:0.4 animations:^{label.alpha = 0.0;} completion:^(BOOL finished) {[label removeFromSuperview];}];
    NSLog(@"Total Points = %d", points);
    [self.view addSubview:label];
    
}


- (UIDynamicItemBehavior *)createPropertiesForItems:(NSArray *)items
{
    UIDynamicItemBehavior *properties = [[UIDynamicItemBehavior alloc] initWithItems:items];
    properties = [[UIDynamicItemBehavior alloc] initWithItems:items];
    properties.friction = 0.0;
    properties.allowsRotation = NO;
    [self.animator addBehavior:properties];
    return properties;
    
}

- (NSArray *)allItems
{
    NSMutableArray *items = [@[self.paddle] mutableCopy];
    return items;
}
//    for (UIView *item in self.balls) [items addObject:item];
//    for (UIView *item in self.bricks) [items addObject:item];
//self.image = [UIImage imageNamed:@"Sword"];



- (void)createPaddle
{
    int paddleHeight = 10;
    self.paddle = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - paddleWidth) / 2, (self.view.frame.size.height - paddleHeight*1.5), paddleWidth, paddleHeight)];
    self.paddle.backgroundColor = [UIColor grayColor];
    self.paddle.layer.cornerRadius = 3;
    [self.view addSubview:self.paddle];
    
    self.attacher = [[UIAttachmentBehavior alloc] initWithItem:self.paddle attachedToAnchor:CGPointMake(CGRectGetMidX(self.paddle.frame), CGRectGetMidY(self.paddle.frame))];
    [self.animator addBehavior:self.attacher];
}

- (void)createBricks
{
    int brickCols = 8;
    int brickRows = 4;
    
    float brickWidth = (SCREEN_WIDTH - (10 * (brickCols + 1))) / brickCols;
    float brickHeight = 20;
    
    for (int row = 0; row < brickRows; row++)
    {
        for (int col = 0; col < brickCols; col++)
        {
            float brickX = ((brickWidth + 10) * col) + 10;
            float brickY = ((brickHeight + 10) * row) + 10;
            
            UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(brickX, brickY, brickWidth, 20)];
            brick.layer.cornerRadius = 10;
            brick.backgroundColor = RED_COLOR;
            
            int random = arc4random_uniform(5) * 50;
            brick.tag = random;
            
            [self.view addSubview:brick];
            [self.bricks addObject:brick];
            [self.collider addItem:brick];
        }
    }
}

- (void)createBall
{
    CGRect frame = self.paddle.frame;
    int ballSize = 12;
    UIImageView *ball = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x+paddleWidth/2-ballSize/2, frame.origin.y-ballSize, ballSize, ballSize)];
    ball.image = [UIImage imageNamed:@"symbol_of_chaos"];
    ball.backgroundColor = [UIColor whiteColor];
    ball.layer.cornerRadius = 6;
    [self.view addSubview:ball];
    [self.balls addObject:ball];
    self.pusher = [[UIPushBehavior alloc] initWithItems:self.balls mode:UIPushBehaviorModeInstantaneous];
    
    [self.collider addItem:ball];
    [self.ballsDynamicProperties addItem:ball];
    
    self.pusher.pushDirection = CGVectorMake(0.015, -0.015);
    self.pusher.active = YES;
    [self.animator addBehavior:self.pusher];
}
// size of the object is interpretted as its mass

// self.attacher = [[UIAttachmentBehavior alloc] initWithItem:self.paddle attachedToAnchor:CGPointMake(CGRectGetMidX(self.paddle.frame), CGRectGetMidY(self.paddle.frame), )];


- (void)tapScreen:(UITapGestureRecognizer *)gr
{
    CGPoint location = [gr locationInView:self.view];
    self.attacher.anchorPoint = CGPointMake(location.x, self.attacher.anchorPoint.y);
}

@end;
