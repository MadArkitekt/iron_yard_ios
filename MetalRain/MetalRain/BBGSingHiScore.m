//
//  BBGSingHiScore.m
//  Data Now App
//
//  Created by MadArkitekt on 5/6/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "BBGSingHiScore.h"

@implementation BBGSingHiScore
+ (BBGSingHiScore *)mainData
{
    static dispatch_once_t create;
    static BBGSingHiScore *data;
    
    dispatch_once(&create, ^{
        data =[[BBGSingHiScore alloc]init];
    });
    return data;
}
- (void)setCurrentScore:(NSInteger)currentScore
{
    _currentScore = currentScore;
    if(currentScore > _topScore)  _topScore = currentScore;
    NSLog(@"Current: %d  Top: %d", _currentScore, _topScore);
}
- (NSArray *)gameScores;
{
    return @[];
}
@end
