//
//  BBGViewController.m
//  MetalRain
//
//  Created by MaddArkitekt on 4/17/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "BBGViewController.h"
#import "BBGLevelController.h"
#import "BBGSingHiScore.h"

@interface BBGViewController () <BBGLevelDelegate>

@end

@implementation BBGViewController
{
    UIButton *startButton;
    UILabel *titleHeader;
    UILabel *scoreBoard;
    NSString *scoreTotal;
    BBGLevelController *level;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    startButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH * .4), (SCREEN_HEIGHT*.3), 120, 120)];
    startButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:30];
    [startButton setTitle:@"PLAY" forState:UIControlStateNormal];
    startButton.layer.cornerRadius = 60;
    startButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:startButton];
    [startButton addTarget:self action:@selector(startButton) forControlEvents:UIControlEventTouchUpInside];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)postPoints;
{
    scoreBoard = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 100, titleHeader.frame.size.height)];
    scoreBoard.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:12];
    scoreBoard.text = [NSString stringWithFormat:@"TOP SCORE: %d", [BBGSingHiScore mainData].topScore];
    scoreBoard.textColor = [UIColor blackColor];
    [titleHeader addSubview:scoreBoard];

}

- (void)startButton
{
    level = [[BBGLevelController alloc] initWithNibName:nil bundle:nil];
    level.delegate = self;
    level.view.frame = CGRectMake(0, 20, SCREEN_WIDTH, (SCREEN_HEIGHT - 20));
    level.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1.0];
    [self.view addSubview:level.view];
    [startButton removeFromSuperview];
    [self drawHeader];

    [level resetLevel];
   
  //  [self drawHeader];
}

-(void)drawHeader
{
    titleHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    titleHeader.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    UIImageView *ball = [[UIImageView alloc] initWithFrame:CGRectMake(400, 5, 12, 12)];
    ball.image = [UIImage imageNamed:@"symbol_of_chaos"];
    ball.layer.cornerRadius = 6;
    [titleHeader addSubview:ball];
    //[self addPoints:0];
    [self.view addSubview:titleHeader];

    
    // [titleHeader addSubview:scoreBoard];


}

- (void)gameDone
{
    NSLog(@"game done");
    [scoreBoard removeFromSuperview];
    [level.view removeFromSuperview];
    [titleHeader removeFromSuperview];
  //  [self viewDidLoad];
    [self.view addSubview:startButton];
}

- (BOOL)prefersStatusBarHidden { return YES; }
@end
