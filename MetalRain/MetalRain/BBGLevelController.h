//
//  BBGLevelController.h
//  MetalRain
//
//  Created by MaddArkitekt on 4/17/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BBGLevelDelegate;

@interface BBGLevelController : UIViewController

@property (nonatomic, assign) id<BBGLevelDelegate> delegate;
- (void)resetLevel;

@end

@protocol BBGLevelDelegate <NSObject>

@optional
- (void)addPoints:(int)points;
- (void)gameDone;

@end