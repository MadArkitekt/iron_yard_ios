//
//  main.m
//  W1D1 App
//
//  Created by MaddArkitekt on 3/31/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESLAppDelegate class]));
    }
}
