//
//  ESLAppDelegate.h
//  W1D1 App
//
//  Created by MaddArkitekt on 3/31/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
