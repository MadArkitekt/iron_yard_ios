//
//  ESLViewController.m
//  W1D1 App
//
//  Created by MaddArkitekt on 3/31/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "ESLViewController.h"

@interface ESLViewController ()

@end

@implementation ESLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)buttonPress:(id)sender {
    
    self.labelOne.text = @"GO!!!";
    self.labelOne.textColor = [UIColor greenColor];
}

- (IBAction)submitButton:(id)sender {
    self.myOutput.text = self.myTextField.text;
   // self.labelOne.textColor = [UIColor blackColor];
}

- (IBAction)blackButton:(id)sender {
    
    self.view.backgroundColor = [UIColor blackColor];
    
}



@end
