//
//  ESLViewController.h
//  W1D1 App
//
//  Created by MaddArkitekt on 3/31/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESLViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelOne;

- (IBAction)buttonPress:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *myTextField;
@property (weak, nonatomic) IBOutlet UILabel *myOutput;
- (IBAction)submitButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *backgroundChange;
- (IBAction)blackButton:(id)sender;


@end
