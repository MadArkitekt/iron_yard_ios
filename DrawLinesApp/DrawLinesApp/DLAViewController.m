//
//  DLAViewController.m
//  DrawLinesApp
//
//  Created by MaddArkitekt on 4/15/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "DLAViewController.h"
#import "DLAStageLines.h"
#import "DLAStageScribble.h"

@interface DLAViewController ()
{
    DLAStageScribble * scribbleView;
    UIView * colorsDrawer;
    
    float lineWidth;
    UIColor *lineColor;
}
@end

@implementation DLAViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
           }
    return self;
}
- (void)colorButtonClicked:(UIButton *)sender
{

}
- (void)viewDidLoad
{

    [super viewDidLoad];
    
    lineWidth = 5.0;
    lineColor = [UIColor lightGrayColor];
    [self toggleStage];
    
    [self.view addSubview:scribbleView];
    
    UISlider * slider = [[UISlider alloc] initWithFrame:CGRectMake(20, SCREEN_HEIGHT - 60, 280, 40)];
    [self.view addSubview:slider];
    
    slider.minimumValue = 2.0;
    slider.maximumValue = 20.0;
    slider.value = lineWidth;
    
    [slider addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventAllEvents];
    
    [self.view addSubview:slider];
    
    colorsDrawer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    NSArray * colors = @[[UIColor colorWithRed:1.000f green:0.478f blue:0.133f alpha:1.0f],
                         [UIColor colorWithRed:0.239f green:1.000f blue:0.341f alpha:1.0f],
                         [UIColor colorWithRed:1.000f green:0.337f blue:0.514f alpha:1.0f],
                         [UIColor colorWithRed:1.000f green:0.957f blue:0.027f alpha:1.0f],
                         [UIColor colorWithRed:0.753f green:0.337f blue:1.000f alpha:1.0f],
                         [UIColor colorWithRed:0.867f green:1.000f blue:0.239f alpha:1.0f],
                         [UIColor colorWithRed:0.035f green:0.953f blue:1.000f alpha:1.0f],
                         [UIColor colorWithRed:0.239f green:1.000f blue:0.561f alpha:1.0f],
                         [UIColor colorWithRed:1.000f green:0.420f blue:0.035f alpha:1.0f],
                         [UIColor colorWithRed:0.698f green:0.286f blue:0.012f alpha:1.0f],
                         [UIColor colorWithRed:0.188f green:0.122f blue:1.000f alpha:1.0f],
                         [UIColor colorWithRed:0.000f green:0.667f blue:0.698f alpha:1.0f]];
    
    
    float buttonWidth = SCREEN_WIDTH / [colors count];
    
    for (UIColor * color in colors)
    {
        int index = [colors indexOfObject:color];
        
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(buttonWidth * index, 0, buttonWidth, 40)];
        button.backgroundColor = color;
        [button addTarget:self action:@selector(changeColor:) forControlEvents:UIControlEventTouchUpInside];
        [colorsDrawer addSubview:button];
    }
    [self.view addSubview:colorsDrawer];
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    UISwitch * toggleSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(255, 65, 80, 30)];
    toggleSwitch.backgroundColor = [UIColor clearColor];
    [toggleSwitch addTarget:self action:@selector(toggleStage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:toggleSwitch];
    
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    UIButton * undoButton = [[UIButton alloc] initWithFrame:CGRectMake(255, 105, 50, 30)];
    undoButton.backgroundColor = [UIColor greenColor];
    undoButton.layer.cornerRadius = 15;
    [undoButton addTarget:self action:@selector(undoStage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:undoButton];

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    UIButton * clearButton = [[UIButton alloc] initWithFrame:CGRectMake(255, 145, 50, 30)];
    clearButton.backgroundColor = [UIColor redColor];
    clearButton.layer.cornerRadius = 15;
    [clearButton addTarget:self action:@selector(clearStage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:clearButton];
}

- (void)undoStage
{
    [scribbleView undoStage];
}

- (void)clearStage
{
    [scribbleView clearStage];
}

- (void)toggleStage
{
    NSMutableArray * lines = scribbleView.lines;
    
    
    
    [scribbleView removeFromSuperview];
    
    
    if ([scribbleView isMemberOfClass:[DLAStageScribble class]])
    {
        scribbleView = [[DLAStageLines alloc] initWithFrame:self.view.frame];
        
    } else {
        
        scribbleView = [[DLAStageScribble alloc] initWithFrame:self.view.frame];
    
    }
    
    if(lines != nil) scribbleView.lines = lines;

    scribbleView.lineWidth = lineWidth;
    scribbleView.lineColor = lineColor;
    
    [self.view insertSubview:scribbleView atIndex:0];
}

- (void)changeSize:(UISlider *)sender
{
    scribbleView.lineWidth = lineWidth;
    lineWidth = sender.value;
    
}

- (void)changeColor:(UIButton *)sender
{
    scribbleView.lineColor = lineColor;
    lineColor = sender.backgroundColor;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
