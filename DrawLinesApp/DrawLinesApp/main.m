//
//  main.m
//  DrawLinesApp
//
//  Created by MaddArkitekt on 4/15/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DLAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DLAAppDelegate class]));
    }
}
