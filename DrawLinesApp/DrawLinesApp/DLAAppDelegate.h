//
//  DLAAppDelegate.h
//  DrawLinesApp
//
//  Created by MaddArkitekt on 4/15/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
