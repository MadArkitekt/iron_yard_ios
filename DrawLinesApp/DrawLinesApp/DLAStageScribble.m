//
//  DLAStagesScribble.m
//  DrawLinesApp
//
//  Created by MaddArkitekt on 4/15/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "DLAStageScribble.h"

@implementation DLAStageScribble

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // defaults
        self.lineWidth = 2.0;
        self.lineColor = [UIColor colorWithWhite:0.3 alpha:1.0];
        
        // init array
        self.lines = [@[]mutableCopy];
    
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)clearStage;
{
    [self.lines removeAllObjects];
    [self setNeedsDisplay];
}
- (void)undoStage;
{
    [self.lines removeLastObject];
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
//    CGContextMoveToPoint(context, 50, 50);
//    CGContextAddCurveToPoint(context, 270, 50, 270, 400, 50, 270);
    CGContextSetLineWidth(context, self.lineWidth); //replaced 5.0 with self.linewidth
//    [self.lineColor set];
//    CGContextClearRect(context, rect);

    [self.lineColor set]; // = a UIColor that we're putting in
    
    for (NSDictionary * line in self.lines) {
        CGContextSetLineWidth(context, [line[@"width"] floatValue]); //replaced 5.0 with self.linewidth
        [(UIColor *)line[@"color"] set];
        
        NSArray * points = line[@"points"];
        
        CGPoint start = [points[0] CGPointValue];
        
        CGContextMoveToPoint(context, start.x, start.y);
        
        for (NSValue * value in points) {
            
            CGPoint point = [value CGPointValue];
            
            CGContextAddLineToPoint(context, point.x, point.y);
            
         //   if (index > 0)
                
                CGContextAddLineToPoint(context, point.x, point.y);
        }
        CGContextStrokePath(context);
    }
}

- (void)setLineColor:(UIColor *)lineColor
{
    _lineColor = lineColor;
    [self setNeedsDisplay];
}

-(void)setLineWidth:(float)lineWidth
{
    _lineWidth = lineWidth;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
for (UITouch * touch in touches) {
    CGPoint location = [touch locationInView:self];

    [self.lines addObject:[@{
                            @"color" : self.lineColor,
                            @"width" : @(self.lineWidth),
                            @"points" : [@[[NSValue valueWithCGPoint:location]] mutableCopy]
                            } mutableCopy]];
    
    /*
     the above allows lines to remain their original color
     */
    
    [self setNeedsDisplay];
    
}
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self doTouch:touches];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self doTouch:touches];
}

- (void)doTouch:(NSSet *)touches
{
    UITouch * touch = [touches allObjects][0];
    
    CGPoint location = [touch locationInView:self];
    
    [[self.lines lastObject][@"points"] addObject:[NSValue valueWithCGPoint:location]];
    
    [self setNeedsDisplay];
    
}

@end
