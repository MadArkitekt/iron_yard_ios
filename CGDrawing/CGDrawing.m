//
//  CGDrawing.m
//  CGDrawing
//
//  Created by MaddArkitekt on 4/17/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CGDrawing.h"

@implementation CGDrawing

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
