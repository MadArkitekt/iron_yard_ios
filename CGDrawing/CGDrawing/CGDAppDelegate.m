//
//  CGDAppDelegate.m
//  CGDrawing
//
//  Created by MaddArkitekt on 4/17/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import "CGDAppDelegate.h"
#import "CGDrawing.h"

@implementation CGDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    UIViewController * viewController = [[UIViewController alloc] initWithNibName:nil bundle:nil];
    
    viewController.view = [[CGDrawing alloc] init];
    
    return YES;
}

@end