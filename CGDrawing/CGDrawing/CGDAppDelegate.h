//
//  CGDAppDelegate.h
//  CGDrawing
//
//  Created by MaddArkitekt on 4/17/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CGDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
