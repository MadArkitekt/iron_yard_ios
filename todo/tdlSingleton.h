//
//  tdlSingleton.h
//  todo
//
//  Created by MadArkitekt on 5/6/14.
//  Copyright (c) 2014 Ed Salter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface tdlSingleton : NSObject

+ (tdlSingleton *)sharedSingleton;
- (void)removeListItem:(NSDictionary *)listItem;
- (void)addListItem:(NSDictionary *)listItem;
- (void)removeListItemAtIndex:(NSInteger) index;
- (NSArray *)allListItems;

@end
