//
//  tdlSingleton.m
//  todo
//
//  Created by MadArkitekt on 5/6/14.
//  Copyright (c) 2014 John Yam. All rights reserved.
//

#import "tdlSingleton.h"

@interface tdlSingleton ( )
@property (nonatomic) NSMutableArray  *listItems;
@end
@implementation tdlSingleton;

+ (tdlSingleton *)sharedSingleton
{
    static dispatch_once_t create;
    static tdlSingleton *singleton = nil;
    
    dispatch_once(&create, ^{
        singleton =[[tdlSingleton alloc]init];
    });
    return singleton;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self loadListItems];
    }
    return self;
}
- (NSMutableArray *)listItems
{
    if(_listItems == nil)
    {
        _listItems = [@[] mutableCopy];
    }
    return _listItems;
}
- (void)addListItem:(NSDictionary *)listItem
{
    [self.listItems addObject:listItem];
    [self saveData];
}

-(NSArray *)allListItems
{
    return [self.listItems copy];
}

-(void)removeListItem:(NSDictionary *)listItem
{
    [self.listItems removeObjectIdenticalTo:listItem];
}
-(void)saveData
{
    NSString *path = [self listArchivePath];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.listItems];
    [data writeToFile:path options:NSDataWritingAtomic error:nil];
}

-(NSString *)listArchivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = documentDirectories[0];
    return [documentDirectory stringByAppendingPathComponent:@"listdata.data"];
    
}

-(void)loadListItems
{
    NSString *path = [self listArchivePath];
    if([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        self.listItems = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    }
}

- (void)removeListeItemAtIndex:(NSInteger)index
{
    [self.listItems removeObjectAtIndex:index];
    [self saveData];
}

@end
